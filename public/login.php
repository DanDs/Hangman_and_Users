<?php
require_once("utilities.php");
   session_start();

   if($_SERVER["REQUEST_METHOD"] == "POST") {
      // username and password sent from form

      $username = $_POST['username'];
      $password = $_POST['password'];
      $conn = createConnection();
      $sql = "SELECT activo FROM user WHERE email = ?  and password = ?";


      $stmt = mysqli_stmt_init($conn);
  	  $activado;
  		if (mysqli_stmt_prepare($stmt, $sql))
  		{
  			mysqli_stmt_bind_param($stmt, "ss", $myusername, $mypassword);
  			mysqli_stmt_execute($stmt);
  			mysqli_stmt_bind_result($stmt, $activado);
  			mysqli_stmt_fetch($stmt);
  			mysqli_stmt_close($stmt);

  			if($activado == 0)
        {
          $sql = "UPDATE `user` SET `activo`= ? WHERE email = ?  and password = ?";


          $stmt = mysqli_stmt_init($conn);
      	  $activo = 1;
      		if (mysqli_stmt_prepare($stmt, $sql))
      		{
      			mysqli_stmt_bind_param($stmt, "iss", $activo, $username, $password);
      			mysqli_stmt_execute($stmt);
      			mysqli_stmt_bind_result($stmt, $activado);
      			mysqli_stmt_fetch($stmt);
      			mysqli_stmt_close($stmt);
            header("location: http://localhost/hangman/public/index.php?user=" .$username);
        }
      }
  		}
      else {
          $error = "Your Login Name or Password is invalid";
          alert($error);
      }
    }

?>


<html>

   <head>
      <title>Login Page</title>


      <meta name="viewport" content="width=device-width, initial-scale=1.0">
      <link href="css/bootstrap.min.css" rel="stylesheet" media="screen">
   </head>

   <body bgcolor = "#FFFFFF">
     <div class="jumbotron" style="text-align: center">
       <h1>Login for Hangman</h1><br>
     </div>

     <form class="form-horizontal" action = "" method = "post" >
        <div class="control-group">
          <label style="margin-left: 35%; margin-right: 20px" class="control-label" name = "username" for="inputEmail">Email</label>
          <div class="controls">
            <input type="text" name = "username" id="inputEmail" placeholder="Email">
          </div>
        </div>
        <div class="control-group">
          <label style="margin-left: 35%; margin-right: 20px" class="control-label" name = "password" for="inputPassword">Password</label>
          <div class="controls">
            <input type="password" name = "password" id="inputPassword" placeholder="Password">
          </div>
        </div>
        <div class="control-group">
          <div class="controls">
            <label style="margin-left: 40%" class="checkbox">
              <input type="checkbox"> Remember me
            </label>
            <input style="margin-left: 40%" type = "submit" value = " Sign in "/>


          </div>
        </div>
      </form>


      <script src="http://code.jquery.com/jquery.js"></script>
      <script src="js/bootstrap.min.js"></script>
   </body>
</html>
