<!DOCTYPE html>
<?php 
   if (isset($_POST["token"]))
   {	
       require_once("stripe-php-3.13.0/init.php");
		
		\Stripe\Stripe::setApiKey("sk_test_5NkSwjR6XTgUAxo96GgO2Zcu");

        // Get the credit card details submitted by the form
	    $token = $_POST['token'];

		// Create the charge on Stripe's servers - this will charge the user's card
		try {
		  $charge = \Stripe\Charge::create(array(
			"amount" => 1000, // amount in cents, again
			"currency" => "usd",
			"source" => $token,
			"description" => "Mi primer cobro"
			));
		} catch(\Stripe\Error\Card $e) {
		  // The card has been declined
		}
   
        print_r($_POST);
   }
?>
<html>
   <head>
      <script src="http://ajax.googleapis.com/ajax/libs/angularjs/1.4.8/angular.min.js">
	  </script>
	  <script src="https://js.stripe.com/v2/"></script>
	  <script type="text/javascript">
         Stripe.setPublishableKey('pk_test_S9FPUiNTXnmrCw0Y2DuDodRa');
       </script>
   </head>
   <body ng-app="myApp" ng-controller="myCtrl">
     <form method="POST" action="">
	    Nombre: <input type="text" name="name">
		<input type="hidden" name="token" ng-value="token" />
	 </form>
   
     <p style="color:red;">{{ errorMessage }}</p>
	 Numero de tarjeta: <input type="text" size="20" ng-model="cardNumber" /><br />
	 Fecha de expiracion: <input type="text" size="4" ng-model="expMonth" />/<input type="text" size="4" ng-model="expYear" /><br />
	 CVC: <input type="text" size="4" ng-model="cvc" /><br />	 
	 
	 Usted pagara 10 $us por la suscripcion.
	 
	 <button ng-click="submit()">Registrarme</button>
	 
	 <script>
	    var app = angular.module("myApp", []);
		
		app.controller("myCtrl", function($scope) {
            $scope.cardNumber = "";
            $scope.cvc = "";
            $scope.expMonth = "";
            $scope.expYear = "";
            $scope.errorMessage	= "";
			$scope.token = "";
		
		    $scope.submit = function() {
				 // Request a token from Stripe:
                 Stripe.card.createToken({
					 number: $scope.cardNumber,
					 cvc: $scope.cvc,
					 exp_month: $scope.expMonth,
					 exp_year: $scope.expYear
				 }, $scope.stripeResponseHandler);
			};
			
			$scope.stripeResponseHandler = function(status, response)
			{
				if (response.error)
				{
					$scope.errorMessage = response.error.message;
					$scope.$apply();
				}
				else
				{
					$scope.token = response.id;
					$scope.$apply();
					
					document.forms[0].submit();
			    }
			}
		});
     </script>	 
   </body>
   
</html>