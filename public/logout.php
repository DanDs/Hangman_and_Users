<!DOCTYPE html>
<?php

  require_once("utilities.php");
  session_start();

  if($_SERVER["REQUEST_METHOD"] == "POST") {
       // username and password sent from form
        $username = htmlspecialchars($_GET["user"]);
       //$username = $_REQUEST['username'];

       $conn = createConnection();

       $stmt = mysqli_stmt_init($conn);
   	   $activado;

       $sql = "UPDATE `user` SET `activo`= ? WHERE email = ?";


       $stmt = mysqli_stmt_init($conn);
       $activo = 0;
       if (mysqli_stmt_prepare($stmt, $sql))
     		{
     			mysqli_stmt_bind_param($stmt, "is", $activo, $username);
     			mysqli_stmt_execute($stmt);
     			mysqli_stmt_bind_result($stmt, $activado);
     			mysqli_stmt_fetch($stmt);
     			mysqli_stmt_close($stmt);

       }
      header("location: 'Login.php'");
       if(session_destroy()) {
          header("Location: http://localhost/hangman/public/login.php");
       }
     }
?>
<html>
<head>
    <title> Log Out </title>
</head>
<body>
  <h1> You are Logged Out</h1>
  <a href="http://localhost/hangman/public/login.php" >Log In</a>
</body>



</html>
