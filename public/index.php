<!DOCTYPE html>

<html lang="es-BO">

	<head>
    <title>Hangman</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
	    <title>El ahorcado</title>
			<?php
				$username = htmlspecialchars($_GET["user"]);
			?>
		<meta charset="UTF-8">
	    <link rel="stylesheet" type="text/css" href="css/main.css">
		<script src="https://ajax.googleapis.com/ajax/libs/angularjs/1.4.10/angular.min.js"></script>
        <style>
            h1{
                text-align: center;
                font-variant: small-caps;
								font-family: "Courier New", Courier, monospace;
                font-size: 50px;
                color: #122729;
            }
            body{
								font-family: "Courier New", Courier, monospace;
                background-color: grey;
            }

            img{
                margin-left: 30%;
            }

						#trybutton{
							font-family: "Courier New", Courier, monospace;
							color: #5D2A2A;
							font-size: 16px;
							text-align: center;
						}

        </style>
	</head>
	<body ng-app="hangman" ng-controller="MainController">
    <h1>Welcome to</h1>
<script src="http://code.jquery.com/jquery.js"></script>
        <h1>Hangman</h1>
				<a href="<?php echo "http://localhost/hangman/public/logout.php?user=" .$username ?>" >Log Out</a>

	    <div>
		    <img class="health" ng-src="{{ 'img/' + health + '.png' }}" style="margin-left: 40%"/>
		</div>

		<div class="userInput" ng-hide="hasFinished()">
		    <div class="revealed" style="margin-left: 40%; font-family: 'Courier New', Courier, monospace;">
			  {{ revealed }}
			</div>

			<form ng-submit="submitLetter()">
				<input style="margin-left: 40%"
                       type ="text"
					   id="letterTextBox"
					   name="letterTextBox"
					   maxlength="1"
					   size="1"
					   ng-model="letter"
					   autofocus />
				<input style="font-variant: small-caps;"
                       type ="submit"
					   id="tryButton"
					   name="tryButton"
					   value="Intentar" />
			</form>
		</div>

		<div ng-show="hasFinished()">
			<div ng-show="hasWon" style="margin-left: 40%; font-variant: small-caps; font-size: 20px">Ganaste</div>
			<div ng-show="!hasWon" style="margin-left: 40%; font-variant: small-caps; font-size: 20px">Perdiste</div>
			<a href="" ng-click="init()" style="margin-left: 40%; font-variant: small-caps; font-size: 20px">Volver a jugar</a>
		</div>
        <script>
		  var app = angular.module("hangman", []);

		  app.controller("MainController", ["$scope", "$http", function($scope, $http) {
			$scope.letter = '';
			$scope.health = 6;
			$scope.revealed = "";
			$scope.hasWon = false;

			$scope.hasFinished = function() {
			   return $scope.health == 0 || $scope.hasWon;
			};

			$scope.init = function() {
			   $http.post('play.php').then( processResponse );
			};

			$scope.submitLetter = function() {
			   var data = { letter: $scope.letter };
			   $http.post('play.php', data).then(processResponse);
			   $scope.letter = '';
			};

			$scope.init();

			function processResponse(response) {
			  $scope.hasWon = response.data.hasWon;
			  $scope.health = response.data.health;
			  $scope.revealed = response.data.revealed;
			}
		  }]);

		</script>
	</body>
</html>
