<?php
function createConnection()
{
	$servername = "localhost";
	$username = "root";
	$password = "";
	$database = "hangman";

	$conn = mysqli_connect($servername, $username, $password, $database);

	if (!$conn)
	{
		die("Fallo en la conexión con la Base de Datos. "
		   . mysqli_connect_error());
	}

	return $conn;
}

function getUserName($usId, $conn)
{
  $sql = "SELECT Name FROM users WHERE idUsuario=?";

    $stmt = mysqli_stmt_init($conn);

    if (mysqli_stmt_prepare($stmt, $sql))
    {
      mysqli_stmt_bind_param($stmt, "i", $usId);
      mysqli_stmt_execute($stmt);
      mysqli_stmt_bind_result($stmt, $Name);
      mysqli_stmt_fetch($stmt);
      mysqli_stmt_close($stmt);

      return $Name;
    }
    else
      return null;
}
?>
