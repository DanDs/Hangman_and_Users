<!DOCTYPE html>
<?php
require_once("utilities.php");


    session_start();

    if(isset($_POST["registeruser"]))
    {
            $allcorrect = 0;
            $email = $_POST["u_email"];
            $password = $_POST["password"];
            $c_password = $_POST["c_password"];
            $full_name = $_POST["full_name"];
            $credit_card_nro = $_POST["credit_card_n"];
            $network_emition = $_POST["network_emition"];
            $credit_card_sc = $_POST["credit_card_sc"];
            $credit_card_ed = $_POST["credit_card_ed"];
            $activo = 0;
            $conn = createConnection();
            $pswd_c = $password;

            if($password == '' && $password != $c_password || $c_password == ''  && $password != $c_password)
            {
                echo '<script language="javascript">alert("no coinciden los passwords");</script>';
                 $allcorrect = $allcorrect + 1;

            }
            if( $allcorrect == 0)
            {
                $sql = "INSERT INTO user(email, password, full_name, credit_card_number, credit_card_emition_network, security_code, cc_expiration_date, activo) VALUES (?, ?, ?, ?, ?, ?, ?, ?)";

                $stmt = mysqli_stmt_init($conn);

                if(mysqli_stmt_prepare($stmt,$sql))
                {
                    mysqli_stmt_bind_param($stmt, "sssdssss", $email, $pswd_c, $full_name, $credit_card_nro, $network_emition, $credit_card_sc, $credit_card_ed, $activo);

                    mysqli_stmt_execute($stmt);

                    $lastId = mysqli_insert_id($conn);
                    //header("location: http://" . $_SERVER["HTTP_HOST"] . );
                    header("Location: http://localhost/hangman/public/login.php?id=" . $lastId);
                //header("Location: http://hangman.local/category.php?id=" . $lastId);
                    $success = true;
                }
            }
        else{
             echo '<script language="javascript">alert("Registro de Usuario Invalido");</script>';
        }


    }



?>
<html>
    <head>
        <title>Register New User</title>
        <meta charset="UTF-8">
        <style>
            body{
                font-family: "Courier New", Courier, monospace;
                background-color: grey;
            }

           p{
                color: #5C280B;
                font-size: 18px;
                font-family: "Courier New", Courier, monospace;
                margin-left: 40%;
           }

            input{
                margin-left: 45%;
            }

            h1{
                text-align: center;
                font-variant: small-caps;
                font-family: "Courier New", Courier, monospace;
                font-size: 50px;
                color: #122729;
            }

        </style>
        <script>

        </script>
    </head>
    <body>
        <h1>New User</h1> 
        <ul>

        </ul>
        <form method="POST" action="new_user.php">
            <p><b>Email: </b></p><input type="email" name="u_email" minlength="6"/>
            <p><b>Password: </b></p><input type="password" name="password" minlength="6"/>
            <p><b>Confirm Password: </b></p><input type="password" name="c_password" minlength="6"/>
            <p><b>Full Name: </b></p><input type="text" name="full_name" maxlength="50"/>
            <p><b>Credit Card N#: </b></p><input type="number" name="credit_card_n" minlength="8"/>
                    <p><b>Credit Card Emition Network: </b></p>
                    <input type="radio" name="network_emition" value="visa" checked> Visa<br>
                    <input type="radio" name="network_emition" value="master_card"> Master Card<br>


            <p><b>Credit Card Security Code: </b></p><input type="number" name="credit_card_sc" minlength="10"/>
            <p><b>Credit Card Expiration Date: </b></p><input type="date" name="credit_card_ed"/>            <br><br>
            <input type="submit" name="registeruser" value="Registrar Usuario" />

        </form>
    </body>
</html>
