-- phpMyAdmin SQL Dump
-- version 4.5.1
-- http://www.phpmyadmin.net
--
-- Servidor: 127.0.0.1
-- Tiempo de generación: 23-05-2016 a las 17:33:21
-- Versión del servidor: 10.1.10-MariaDB
-- Versión de PHP: 7.0.4

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `hangman`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `category`
--

CREATE TABLE `category` (
  `id` int(11) NOT NULL,
  `name` varchar(30) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `category`
--

INSERT INTO `category` (`id`, `name`) VALUES
(1, 'Accion'),
(2, 'Comedias'),
(3, 'Terror'),
(4, 'Suspenso'),
(6, 'Extranjeros'),
(7, 'Otros'),
(8, 'Ciencia Ficcion'),
(11, 'Anime'),
(12, 'Peliculas'),
(16, 'Juanito'),
(17, 'Pepe'),
(29, 'nada que ver');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `movies`
--

CREATE TABLE `movies` (
  `title` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `movies`
--

INSERT INTO `movies` (`title`) VALUES
('pepe'),
('Pocahontas'),
('Mulan'),
('Mushu'),
('Popeye'),
('Vacas locas'),
('Titanic'),
('a'',(asd'),
('Wall - E');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `user`
--

CREATE TABLE `user` (
  `email` varchar(20) NOT NULL,
  `password` varchar(10) NOT NULL,
  `full_name` varchar(50) DEFAULT NULL,
  `credit_card_number` int(15) DEFAULT NULL,
  `credit_card_emition_network` varchar(20) DEFAULT NULL,
  `security_code` int(10) DEFAULT NULL,
  `cc_expiration_date` date DEFAULT NULL,
  `activo` tinyint(1) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `user`
--

INSERT INTO `user` (`email`, `password`, `full_name`, `credit_card_number`, `credit_card_emition_network`, `security_code`, `cc_expiration_date`, `activo`) VALUES
('hola@mail.com', '123456789', 'Juan Pepe Flores', 2147483647, 'visa', 2147483647, '2016-05-21', 1),
('juna@hotmail.com', '456456456', 'pepe jaime', 123123123, 'visa', 123123123, '2016-11-26', 0),
('pepegrillo@gmail.com', 'pepegrillo', 'Pepe Carlos Grillo Monolito', 987654321, 'master_card', 421254135, '2017-05-19', 0);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `word`
--

CREATE TABLE `word` (
  `id` int(11) NOT NULL,
  `text` varchar(50) NOT NULL,
  `categoryId` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `word`
--

INSERT INTO `word` (`id`, `text`, `categoryId`) VALUES
(1, 'Pocahontas', 2),
(2, 'Bambi', 2),
(4, 'Peter Pan', 0),
(7, 'Buscando a Nemo', 0),
(8, 'Mary Poppins', 0),
(28, 'Tarzan', 2),
(29, 'El Rey Leon', 2),
(30, 'Mulan', 2),
(31, 'Terminator', 0),
(33, 'Transformers', 8),
(36, 'nose', 0),
(37, 'Naruto', 0),
(38, 'adfsaf', 0),
(39, 'Tarzan', 0),
(40, 'nose', 2),
(41, 'Planeta del Tesoro', 12),
(43, 'Pepito Grillo', 28),
(45, 'Juanito1', 16),
(46, 'Juanito2', 16),
(47, 'Juanito3', 16),
(48, 'Terror1', 3),
(49, 'Terror2', 3),
(50, 'Terror3', 3),
(51, 'Terror4', 3),
(52, 'Terror5', 3),
(53, 'Suspenso1', 4),
(54, 'Suspenso2', 4),
(55, 'Suspenso3', 4),
(56, 'Ex1', 6),
(57, 'Ex2', 6),
(58, 'Ex3', 6),
(59, 'nose1', 7),
(60, 'nose2', 7),
(61, 'nose3', 7),
(63, 'Naruto', 11),
(64, 'Bleach', 11),
(65, 'Digimon', 11),
(66, 'pepe1', 17),
(67, 'pepe2', 17),
(68, 'pepe3', 17),
(69, 'accion1', 1),
(70, 'accion2', 1),
(71, 'no existe este', 29);

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `category`
--
ALTER TABLE `category`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `user`
--
ALTER TABLE `user`
  ADD PRIMARY KEY (`email`);

--
-- Indices de la tabla `word`
--
ALTER TABLE `word`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `category`
--
ALTER TABLE `category`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=30;
--
-- AUTO_INCREMENT de la tabla `word`
--
ALTER TABLE `word`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=72;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
